package main

// import local cmd
import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"monapi/api"

	oapimiddleware "github.com/deepmap/oapi-codegen/pkg/middleware"
	prometheusmiddleware "github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	"gopkg.in/yaml.v2"
)

// Configuration the HTTP server configuration
type Configuration struct {
	Api api.ApiConfiguration `yaml:"api"`
}

func main() {
	// create logger
	logger := log.New(os.Stdout, "monapi: ", log.LstdFlags|log.Lshortfile)

	// load configuration
	var config Configuration
	config.Api.ResponseDelay = 3 * time.Second
	config.Api.ResponseSize = 26
	file, err := os.ReadFile("config.yaml")
	if err != nil {
		fmt.Fprintf(os.Stdout, "unable to find a configuration file : %s\n", err)
		fmt.Fprintf(os.Stdout, "using default values\n")
	} else {
		if err := yaml.Unmarshal(file, &config); err != nil {
			fmt.Fprintf(os.Stderr, "Fail to read the yaml config file : %s\n", err)
			os.Exit(1)
		}
	}

	// load swagger spec
	swagger, err := api.GetSwagger()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error loading swagger spec : %s\n", err)
		os.Exit(1)
	}
	// Clear out the servers array in the swagger spec, that skips validating
	// that server names match. We don't know how this thing will be run.
	swagger.Servers = nil

	// Create an instance of our handler which satisfies the generated interface
	monapi := api.NewMonapiAPI(config.Api)

	// create echo instance
	logger.Println("creating echo instance")
	e := echo.New()

	// Log all requests
	e.Use(echomiddleware.Logger())

	// Use our validation middleware to check all requests against the
	// OpenAPI schema.
	e.Use(oapimiddleware.OapiRequestValidator(swagger))
	// We now register our api above as the handler for the interface
	api.RegisterHandlers(e, monapi)

	// Create Prometheus server and Middleware
	p := prometheusmiddleware.NewPrometheus("monapi", nil)
	echoPrometheus := echo.New()
	echoPrometheus.HideBanner = true
	// Scrape metrics from Main Server
	e.Use(p.HandlerFunc)
	// Setup metrics endpoint at another server
	p.SetMetricsPath(echoPrometheus)

	// Start Prometheus metrics server
	go func() {
		logger.Println("starting metrics echo instance")
		echoPrometheus.Logger.Fatal(echoPrometheus.Start(":9090"))
		logger.Println("exiting metrics echo instance")
	}()

	// Start Main server
	go func() {
		logger.Println("starting monapi echo instance")
		e.Logger.Fatal(e.Start(":8080"))
		logger.Println("exiting monapi echo instance")
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}
